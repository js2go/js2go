# js2go

## Original Goal

This project started to enable high-performance server-side rendering for [Vue](https://vuejs.org/) for backends written in Go
by compiling JavaScript into Go.
There were two main ideas behind it:
* Compiling to Go *should* be better than other approaches (e.g., using node in a separate process
  just for serving server-rendered content, or embedding the V8 engine) because there would not be any inter-process
  communication (and no overhead to convert between V8 structures and host structures). We could also take
  advantage of the threading model of the Go runtime to render many requests in parallel.
* Curiosity on how would an Ahead-of-Time compiler do in comparison with existing Just-In-Time compiler (V8).
* Many JavaScript features are relatively easy to implement in Go because Go provides out of the box a wide range of powerful
  features. If not easy, it is at least an interesting exercise to do.

## Current Status

After enough JavaScript features have been implemented to do a benchmark, [the benchmark](#benchmark) has shown
that a compiled Go implementation is substantially slower than what V8 provides out of the box.
Go compiler is not doing enough (any?) magic for a relatively straightforward implementation to achieve comparable performance.

The Just-In-Time compilation used by V8 seems to be more suitable than Ahead-of-Time for a dynamically typed language like
JavaScript.
The code produced by js2go has not (yet) been optimized,
but the performance issue seems to be more fundamental:
JavaScript (and current corresponding Go code made by js2go) lacks typing
information for its values which means that every operation
on values has first to check what the type is. In Go, values are
implemented using an interface, which means one pointer lookup
based on the concrete value type.
Moreover, JavaScript
arrays are sparse, so a naive implementation with Go `map` does not
help much with cache locality and adds one more pointer lookup.
You can read more about some issues compiling JavaScript with Ahead-of-Time approach [here](https://news.ycombinator.com/item?id=10228904).

This compiler does produce programs which have more consistent
performance
behavior and lower memory footprint than V8 (node.js) though.
But it is unclear if that would persist once all features of JavaScript
were implemented.

There are few directions this projects could go:

* Do more work on optimizing produced code: do static analysis to detect
  types, use specialized code for those types, find ways to remove pointers
  when they are not needed. But probably we would eventually have to do
  analysis and optimization during runtime, which means we would gain
  some form of JIT. V8 already has all of
  that so it unclear what would be the benefit of this direction.
* The input to this compiler could be TypeScript and compiler could then
  use typing information to produce optimized code. Adding typing information would then have additional performance benefit. Moreover,
  if optimized code would fail at runtime because real values would not
  match expected types, failing execution would be seen as a feature, not
  a bug.
* The compiler could become a JavaScript to Go migration tool, which would
  not really produce fully JavaScript compliant or even executable Go code,
  but idiomatic Go code which might require some manual changes to get it
  to work and behave like original JavaScript code.

Currently this project is on hold and none of these directions are
pursued. If you are interested in contributing or discussing,
feel free to open an issue.

## Installation

Clone with `--recursive`:

```
git clone --recursive https://gitlab.com/js2go/js2go.git
```

If you want to regenerate the JavaScript Go parser, you have to
[install Antlr](https://www.antlr.org/download.html):

```
apt-get install openjdk-14-jre
cd /usr/local/lib
wget https://www.antlr.org/download/antlr-4.9.2-complete.jar
```

Add to `.profile`:

```
export CLASSPATH=".:/usr/local/lib/antlr-4.9.2-complete.jar:$CLASSPATH"
```

Add to `.bashrc`:

```
alias antlr4='java -Xmx500M -cp "/usr/local/lib/antlr-4.9-complete.jar:$CLASSPATH" org.antlr.v4.Tool'
alias grun='java -Xmx500M -cp "/usr/local/lib/antlr-4.9-complete.jar:$CLASSPATH" org.antlr.v4.gui.TestRig'
```

## Benchmark

Benchmark comparing run time of running [`spectralnorm.js`](./benchmark/spectralnorm.js)
(see [description](https://benchmarksgame-team.pages.debian.net/benchmarksgame/description/spectralnorm.html#spectralnorm))
program for various values of `n` for:
* Go v1.16 using [Go version of the program](./benchmark/go/spectralnorm.go)
* Go v1.16 using [Go version of the program](./benchmark/go/spectralnorm.go) and [gccgo compiler](https://golang.org/doc/install/gccgo)
* node.js v16.0.0
* js2go (this compiler) `e9bdcac`
* [Otto](https://github.com/robertkrimen/otto) `ef014fd`
* [v8go](https://github.com/rogchap/v8go) v0.5.1


| *n*  | go         | gccgo       | node        | js2go         | otto           | v8go        |
|------|------------|-------------|-------------|---------------|----------------|-------------|
| 200  |0.006±0.000 | 0.017±0.001 | 0.048±0.003 | 0.505±0.009   | 17.153±0.016   | 0.029±0.001 |
| 400  |0.025±0.002 | 0.030±0.002 | 0.054±0.000 | 1.955±0.011   | 68.992±0.060   | 0.046±0.003 |
| 600  |0.055±0.004 | 0.048±0.004 | 0.074±0.003 | 4.340±0.020   | 157.690±0.369  | 0.062±0.004 |
| 800  |0.089±0.004 | 0.065±0.004 | 0.091±0.000 | 7.732±0.037   | 282.356±0.361  | 0.087±0.003 |
| 1000 |0.128±0.003 | 0.086±0.005 | 0.120±0.000 | 11.890±0.035  | 439.680±1.220  | 0.113±0.003 |
| 1200 |0.175±0.003 | 0.107±0.004 | 0.154±0.000 | 17.165±0.018  | 633.312±0.615  | 0.150±0.003 |
| 1400 |0.243±0.003 | 0.138±0.005 | 0.194±0.000 | 23.501±0.043  | 869.256±0.351  | 0.191±0.004 |
| 1600 |0.314±0.003 | 0.172±0.003 | 0.241±0.000 | 30.883±0.054  | 1143.040±1.380 | 0.238±0.003 |
| 1800 |0.392±0.003 | 0.209±0.004 | 0.294±0.000 | 38.898±0.073  | 1489.740±1.170 | 0.293±0.003 |
| 2000 |0.483±0.003 | 0.257±0.003 | 0.361±0.003 | 48.171±0.037  | 1855.270±4.590 | 0.350±0.003 |
| 2200 |0.569±0.003 | 0.299±0.003 | 0.419±0.000 | 58.508±0.153  |                | 0.417±0.002 |
| 2400 |0.683±0.003 | 0.361±0.003 | 0.496±0.004 | 69.746±0.113  |                | 0.497±0.005 |
| 2600 |0.796±0.003 | 0.410±0.003 | 0.568±0.001 | 81.806±0.075  |                | 0.583±0.003 |
| 2800 |0.924±0.003 | 0.468±0.004 | 0.658±0.003 | 95.581±0.130  |                | 0.664±0.003 |
| 3000 |1.067±0.005 | 0.535±0.004 | 0.743±0.000 | 109.910±0.069 |                | 0.763±0.003 |
| 3200 |1.215±0.003 | 0.598±0.004 | 0.845±0.003 | 125.626±0.111 |                | 0.872±0.004 |
| 3400 |1.358±0.003 | 0.682±0.004 | 0.942±0.000 | 142.237±0.223 |                | 0.987±0.003 |
| 3600 |1.523±0.003 | 0.764±0.003 | 1.054±0.002 | 159.263±0.213 |                | 1.125±0.007 |
| 3800 |1.700±0.002 | 0.839±0.004 | 1.179±0.005 | 177.440±0.228 |                | 1.263±0.006 |
| 4000 |1.887±0.003 | 0.946±0.017 | 1.287±0.000 | 196.930±0.198 |                | 1.403±0.009 |
| 4200 |2.068±0.003 | 1.017±0.004 | 1.427±0.001 | 217.336±0.242 |                | 1.561±0.012 |
| 4400 |2.267±0.002 | 1.115±0.004 | 1.561±0.000 | 239.315±0.449 |                | 1.770±0.013 |
| 4600 |2.479±0.003 | 1.215±0.004 | 1.697±0.003 | 261.922±0.434 |                | 1.983±0.008 |
| 4800 |2.698±0.006 | 1.329±0.006 | 1.858±0.003 | 285.018±0.413 |                | 2.141±0.014 |
| 5000 |2.928±0.003 | 1.435±0.003 | 1.999±0.003 | 313.531±0.481 |                | 2.402±0.018 |

All measurements are in seconds. Repeated 10 times and averaged. Note that measurements
include program startup time.

node.js is really fast and even Go version of the program is unable to keep with it for large `n`.
Only when compiled with gccgo Go compiler, Go version is faster for all `n` than node.js.
It is important to note that Go version of the program knows data types and can preallocate
arrays and that node.js generally has a higher startup cost.
It is amazing to see how node.js can detect and adapt to that on the fly.

Otto is dramatically slow. js2go is visibly slower at small `n` already.

v8go is comparable with the node.js version, but this benchmark has not really transmitted
a lot of data between Go and V8 engine.

## Related Projects

* [Godzilla](https://github.com/owenthereal/godzilla) is a ES2015 to Go
  source code transpiler and runtime, but it seems it does not work on
  anything than the most trivial JavaScript programs.
* [Otto](https://github.com/robertkrimen/otto) is a JavaScript interpreter
  written in Go.
* [v8go](https://github.com/rogchap/v8go) allows running JavaScript
  from Go inside a V8 engine.
* [Gojis](https://github.com/gojisvm/gojis) is another JavaScript interpreter.
* [Goja](https://github.com/dop251/goja) is another JavaScript interpreter.