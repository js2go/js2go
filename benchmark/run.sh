#!/bin/bash

for p in go gccgo node js2go otto v8go ; do
  for i in $(seq 200 200 5000); do
    perf stat -r 10 -B ../spectralnorm-$p $i $i
  done
done