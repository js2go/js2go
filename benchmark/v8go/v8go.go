package main

import (
	"fmt"
	"log"
	"os"

	"rogchap.com/v8go"

	"gitlab.com/js2go/js2go/benchmark"
)

func main() {
	iso, _ := v8go.NewIsolate()
	ctx, _ := v8go.NewContext(iso)

	global := ctx.Global()

	process, _ := v8go.NewObjectTemplate(iso)
	argv, _ := v8go.NewObjectTemplate(iso)
	for i, arg := range os.Args {
		argv.Set(fmt.Sprint(i), arg)
	}
	process.Set("argv", argv)
	processObj, _ := process.NewInstance(ctx)
	global.Set("process", processObj)

	console, _ := v8go.NewObjectTemplate(iso)
	logfn, _ := v8go.NewFunctionTemplate(iso, func(info *v8go.FunctionCallbackInfo) *v8go.Value {
		fmt.Println(info.Args()[0])
		return nil
	})
	console.Set("log", logfn)
	consoleObj, _ := console.NewInstance(ctx)
	global.Set("console", consoleObj)

	_, err := ctx.RunScript(benchmark.Spectralnorm, "spectralnorm.js")
	if err != nil {
		log.Fatalf("ERROR: %s\n", err)
	}
}
