package main

import __js "gitlab.com/js2go/js2go/js"
import __console "gitlab.com/js2go/js2go/js/console"
import __math "gitlab.com/js2go/js2go/js/math"
import __process "gitlab.com/js2go/js2go/js/process"

var console __js.Value = __console.Console
var Math __js.Value = __math.Math
var process __js.Value = __process.Process
var undefined __js.Value = __js.UNDEFINED

func A(i __js.Value, j __js.Value) __js.Value {
	return __js.Divide(__js.Number(1), (__js.Plus(__js.Plus(__js.Divide(__js.Multiply((__js.Plus(i, j)), (__js.Plus(__js.Plus(i, j), __js.Number(1)))), __js.Number(2)), i), __js.Number(1))))
}
func Au(u __js.Value, v __js.Value) __js.Value {
	for i := __js.Value(__js.Number(0)); __js.GoBool(__js.Less(i, u.Get(__js.String("length")))); __js.PreIncrement(&i) {
		var t __js.Value = __js.Number(0)
		for j := __js.Value(__js.Number(0)); __js.GoBool(__js.Less(j, u.Get(__js.String("length")))); __js.PreIncrement(&j) {
			__js.AssignWithOperator(&t, __js.Plus, __js.Multiply(A(i, j), u.Get(j)))
		}
		v.Set(i, t)
	}
	return __js.UNDEFINED
}
func Atu(u __js.Value, v __js.Value) __js.Value {
	for i := __js.Value(__js.Number(0)); __js.GoBool(__js.Less(i, u.Get(__js.String("length")))); __js.PreIncrement(&i) {
		var t __js.Value = __js.Number(0)
		for j := __js.Value(__js.Number(0)); __js.GoBool(__js.Less(j, u.Get(__js.String("length")))); __js.PreIncrement(&j) {
			__js.AssignWithOperator(&t, __js.Plus, __js.Multiply(A(j, i), u.Get(j)))
		}
		v.Set(i, t)
	}
	return __js.UNDEFINED
}
func AtAu(u __js.Value, v __js.Value, w __js.Value) __js.Value {
	Au(u, w)
	Atu(w, v)
	return __js.UNDEFINED
}
func spectralnorm(n __js.Value) __js.Value {
	var i __js.Value = __js.UNDEFINED
	var u __js.Value = __js.NewArray()
	var v __js.Value = __js.NewArray()
	var w __js.Value = __js.NewArray()
	var vv __js.Value = __js.Number(0)
	var vBv __js.Value = __js.Number(0)
	for __js.Assign(&i, __js.Number(0)); __js.GoBool(__js.Less(i, n)); __js.PreIncrement(&i) {
		u.Set(i, __js.Number(1))
		v.Set(i, w.Set(i, __js.Number(0)))
	}
	for __js.Assign(&i, __js.Number(0)); __js.GoBool(__js.Less(i, __js.Number(10))); __js.PreIncrement(&i) {
		AtAu(u, v, w)
		AtAu(v, u, w)
	}
	for __js.Assign(&i, __js.Number(0)); __js.GoBool(__js.Less(i, n)); __js.PreIncrement(&i) {
		__js.AssignWithOperator(&vBv, __js.Plus, __js.Multiply(u.Get(i), v.Get(i)))
		__js.AssignWithOperator(&vv, __js.Plus, __js.Multiply(v.Get(i), v.Get(i)))
	}
	return __js.GoFunction(Math.Get(__js.String("sqrt")))(__js.Divide(vBv, vv))
}
func init() {
	__js.GoFunction(console.Get(__js.String("log")))(__js.GoFunction(spectralnorm(__js.UnaryPlus(process.Get(__js.String("argv")).Get(__js.Number(2)))).Get(__js.String("toFixed")))(__js.Number(9)))
}
