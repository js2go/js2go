package main

import (
	"log"
	"os"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/console"
	"github.com/dop251/goja_nodejs/require"

	"gitlab.com/js2go/js2go/benchmark"
)

func main() {
	registry := new(require.Registry)
	vm := goja.New()
	registry.Enable(vm)
	console.Enable(vm)
	err := vm.Set("process", map[string]interface{}{
		"argv": os.Args,
	})
	if err != nil {
		log.Fatalf("failed to set process: %s", err)
	}
	_, err = vm.RunString(benchmark.Spectralnorm)
	if err != nil {
		log.Fatalf("failed to run: %s", err)
	}
}
