#!/usr/bin/python3

import collections
import re

with open('raw-measurements.txt', 'r', encoding='utf8') as file:
  lines = file.readlines()

header = []
measurements = collections.defaultdict(list)

for i in range(0, int(len(lines) / 24)):
  chunk = lines[i*24:(i+1)*24]
  title, size = re.search('-(\S+) (\d+)', chunk[11]).groups()
  avg, dev = re.search('([0-9.+]+) \+- ([0-9.+]+)', chunk[22]).groups()
  if title not in header:
    header.append(title)
  measurements[int(size)].append((float(avg), float(dev)))

print("|  | " + " | ".join(header) + " |")
print("|--|-" + ("-|-" * (len(header) - 1)) + "-|")

for size in sorted(measurements.keys()):
  numbers = ["%.3f±%.3f" % n for n in measurements[size]]
  print(f"| {size} |" + " | ".join(numbers) + " |")
