package main

import (
	"log"
	"os"

	"github.com/robertkrimen/otto"

	"gitlab.com/js2go/js2go/benchmark"
)

func main() {
	vm := otto.New()
	err := vm.Set("process", map[string]interface{}{
		"argv": os.Args,
	})
	if err != nil {
		log.Fatalf("failed to set process: %s", err)
	}
	_, err = vm.Run(benchmark.Spectralnorm)
	if err != nil {
		log.Fatalf("failed to run: %s", err)
	}
}
