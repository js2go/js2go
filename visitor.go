package js2go

import (
	"fmt"
	"go/format"

	"github.com/antlr/antlr4/runtime/Go/antlr"

	"gitlab.com/js2go/js2go/parser"
)

type VisitorError struct {
	Line   int
	Column int
	Text   string
}

type CompileError struct {
	Message       string
	VisitorErrors []VisitorError
	SyntaxErrors  []SyntaxError
}

func (e *CompileError) Error() string {
	return e.Message
}

func appendWithNewline(base, next string) string {
	result := base
	if len(next) > 0 {
		if len(result) > 0 {
			result += "\n"
		}
		result += next
	}
	return result
}

func appendWithComma(base, next string) string {
	result := base
	if len(next) > 0 {
		if len(result) > 0 {
			result += ", "
		}
		result += next
	}
	return result
}

type Visitor struct {
	Errors             []VisitorError
	TopLevelStatements string
	inForLoop          bool
	atTopLevel         bool
	seenReturn         []bool
}

func (v *Visitor) AcceptStringVisitorResult(tree antlr.ParseTree) parser.StringVisitorResult {
	return parser.StringVisitorResult(v.AcceptString(tree))
}

func (v *Visitor) AcceptString(tree antlr.ParseTree) string {
	return tree.Accept(v).(parser.VisitorResult).String()
}

func (v *Visitor) Visit(tree antlr.ParseTree) interface{} {
	return v.AcceptStringVisitorResult(tree)
}

func (v *Visitor) VisitChildren(node antlr.RuleNode) interface{} {
	result := ""
	for _, tree := range node.GetChildren() {
		res := v.AcceptString(tree.(antlr.ParseTree))
		result = appendWithNewline(result, res)
	}
	return parser.StringVisitorResult(result)
}

func (v *Visitor) VisitTerminal(node antlr.TerminalNode) interface{} {
	return parser.StringVisitorResult(node.GetSymbol().GetText())
}

func (v *Visitor) VisitErrorNode(node antlr.ErrorNode) interface{} {
	v.Errors = append(v.Errors, VisitorError{
		Line:   node.GetSymbol().GetLine(),
		Column: node.GetSymbol().GetColumn(),
		Text:   node.GetText(),
	})
	return parser.StringVisitorResult("")
}

// Visit a parse tree produced by JavaScriptParser#program.
func (v *Visitor) VisitProgram(ctx *parser.ProgramContext) parser.VisitorResult {
	v.atTopLevel = true

	result := v.AcceptStringVisitorResult(ctx.SourceElements())

	v.atTopLevel = false

	return result
}

// Visit a parse tree produced by JavaScriptParser#sourceElement.
func (v *Visitor) VisitSourceElement(ctx *parser.SourceElementContext) parser.VisitorResult {
	return v.AcceptStringVisitorResult(ctx.Statement())
}

// Visit a parse tree produced by JavaScriptParser#statement.
func (v *Visitor) VisitStatement(ctx *parser.StatementContext) parser.VisitorResult {
	atTopLevel := v.atTopLevel
	v.atTopLevel = false

	result := v.VisitChildren(ctx).(parser.VisitorResult).String()

	if atTopLevel {
		if ctx.VariableStatement() != nil || ctx.ImportStatement() != nil || ctx.ExportStatement() != nil || ctx.EmptyStatement_() != nil || ctx.ClassDeclaration() != nil || ctx.DebuggerStatement() != nil || ctx.FunctionDeclaration() != nil {
			// Continue.
		} else {
			v.TopLevelStatements = appendWithNewline(v.TopLevelStatements, result)
			v.atTopLevel = atTopLevel
			return parser.StringVisitorResult("")
		}
	}

	v.atTopLevel = atTopLevel

	return parser.StringVisitorResult(result)
}

// Visit a parse tree produced by JavaScriptParser#block.
func (v *Visitor) VisitBlock(ctx *parser.BlockContext) parser.VisitorResult {
	return v.AcceptStringVisitorResult(ctx.StatementList())
}

// Visit a parse tree produced by JavaScriptParser#statementList.
func (v *Visitor) VisitStatementList(ctx *parser.StatementListContext) parser.VisitorResult {
	result := ""
	allStatement := ctx.AllStatement()
	for _, statement := range allStatement {
		value := v.AcceptString(statement)
		result = appendWithNewline(result, value)
	}
	return parser.StringVisitorResult(result)
}

// Visit a parse tree produced by JavaScriptParser#importStatement.
func (v *Visitor) VisitImportStatement(ctx *parser.ImportStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#importFromBlock.
func (v *Visitor) VisitImportFromBlock(ctx *parser.ImportFromBlockContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#moduleItems.
func (v *Visitor) VisitModuleItems(ctx *parser.ModuleItemsContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#importDefault.
func (v *Visitor) VisitImportDefault(ctx *parser.ImportDefaultContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#importNamespace.
func (v *Visitor) VisitImportNamespace(ctx *parser.ImportNamespaceContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#importFrom.
func (v *Visitor) VisitImportFrom(ctx *parser.ImportFromContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#aliasName.
func (v *Visitor) VisitAliasName(ctx *parser.AliasNameContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ExportDeclaration.
func (v *Visitor) VisitExportDeclaration(ctx *parser.ExportDeclarationContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ExportDefaultDeclaration.
func (v *Visitor) VisitExportDefaultDeclaration(ctx *parser.ExportDefaultDeclarationContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("var DefaultExport = %s", value))
}

// Visit a parse tree produced by JavaScriptParser#exportFromBlock.
func (v *Visitor) VisitExportFromBlock(ctx *parser.ExportFromBlockContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#declaration.
func (v *Visitor) VisitDeclaration(ctx *parser.DeclarationContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#variableStatement.
func (v *Visitor) VisitVariableStatement(ctx *parser.VariableStatementContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#variableDeclarationList.
func (v *Visitor) VisitVariableDeclarationList(ctx *parser.VariableDeclarationListContext) parser.VisitorResult {
	inForLoop := v.inForLoop
	v.inForLoop = false

	result := ""
	for _, iVariableDeclaration := range ctx.AllVariableDeclaration() {
		declaration := iVariableDeclaration.(*parser.VariableDeclarationContext)
		name := v.AcceptString(declaration.Assignable())
		expression := declaration.SingleExpression()
		if expression != nil {
			value := v.AcceptString(expression)
			if inForLoop {
				result = appendWithNewline(result, fmt.Sprintf("%s := __js.Value(%s)", name, value))
			} else {
				result = appendWithNewline(result, fmt.Sprintf("var %s __js.Value = %s", name, value))
			}
		} else {
			if inForLoop {
				result = appendWithNewline(result, fmt.Sprintf("%s := __js.Value(__js.UNDEFINED)", name))
			} else {
				result = appendWithNewline(result, fmt.Sprintf("var %s __js.Value = __js.UNDEFINED", name))
			}
		}
	}

	v.inForLoop = inForLoop

	return parser.StringVisitorResult(result)
}

// Visit a parse tree produced by JavaScriptParser#variableDeclaration.
func (v *Visitor) VisitVariableDeclaration(ctx *parser.VariableDeclarationContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#emptyStatement_.
func (v *Visitor) VisitEmptyStatement_(ctx *parser.EmptyStatement_Context) parser.VisitorResult {
	return parser.StringVisitorResult("")
}

// Visit a parse tree produced by JavaScriptParser#expressionStatement.
func (v *Visitor) VisitExpressionStatement(ctx *parser.ExpressionStatementContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#ifStatement.
func (v *Visitor) VisitIfStatement(ctx *parser.IfStatementContext) parser.VisitorResult {
	condition := v.AcceptString(ctx.ExpressionSequence())
	first := v.AcceptString(ctx.Statement(0))
	secondStatement := ctx.Statement(1)
	if secondStatement != nil {
		second := v.AcceptString(secondStatement)
		return parser.StringVisitorResult(fmt.Sprintf("if __js.GoBool(%s) {\n%s\n} else {\n%s\n}", condition, first, second))
	} else {
		return parser.StringVisitorResult(fmt.Sprintf("if __js.GoBool(%s) {\n%s\n}", condition, first))
	}
}

// Visit a parse tree produced by JavaScriptParser#DoStatement.
func (v *Visitor) VisitDoStatement(ctx *parser.DoStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#WhileStatement.
func (v *Visitor) VisitWhileStatement(ctx *parser.WhileStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ForStatement.
func (v *Visitor) VisitForStatement(ctx *parser.ForStatementContext) parser.VisitorResult {
	offset := 0
	first := ""
	variableDeclaration := ctx.VariableDeclarationList()
	if variableDeclaration != nil {
		v.inForLoop = true
		first = v.AcceptString(variableDeclaration)
		v.inForLoop = false
	} else {
		offset = 1
		firstExpression := ctx.ExpressionSequence(0)
		if firstExpression != nil {
			first = v.AcceptString(firstExpression)
		}
	}

	second := ""
	secondExpression := ctx.ExpressionSequence(0 + offset)
	if secondExpression != nil {
		second = v.AcceptString(secondExpression)
	}

	third := ""
	thirdExpression := ctx.ExpressionSequence(1 + offset)
	if thirdExpression != nil {
		third = v.AcceptString(thirdExpression)
	}

	return parser.StringVisitorResult(fmt.Sprintf("for %s; __js.GoBool(%s); %s {\n%s\n}", first, second, third, v.AcceptString(ctx.Statement())))
}

// Visit a parse tree produced by JavaScriptParser#ForInStatement.
func (v *Visitor) VisitForInStatement(ctx *parser.ForInStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ForOfStatement.
func (v *Visitor) VisitForOfStatement(ctx *parser.ForOfStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#varModifier.
func (v *Visitor) VisitVarModifier(ctx *parser.VarModifierContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#continueStatement.
func (v *Visitor) VisitContinueStatement(ctx *parser.ContinueStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#breakStatement.
func (v *Visitor) VisitBreakStatement(ctx *parser.BreakStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#returnStatement.
func (v *Visitor) VisitReturnStatement(ctx *parser.ReturnStatementContext) parser.VisitorResult {
	// Mark that we have seen a return statement in the current function.
	v.seenReturn[len(v.seenReturn)-1] = true

	value := v.AcceptString(ctx.ExpressionSequence())
	if len(value) > 0 {
		return parser.StringVisitorResult(fmt.Sprintf("return %s", value))
	} else {
		return parser.StringVisitorResult("return __js.UNDEFINED")
	}
}

// Visit a parse tree produced by JavaScriptParser#yieldStatement.
func (v *Visitor) VisitYieldStatement(ctx *parser.YieldStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#withStatement.
func (v *Visitor) VisitWithStatement(ctx *parser.WithStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#switchStatement.
func (v *Visitor) VisitSwitchStatement(ctx *parser.SwitchStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#caseBlock.
func (v *Visitor) VisitCaseBlock(ctx *parser.CaseBlockContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#caseClauses.
func (v *Visitor) VisitCaseClauses(ctx *parser.CaseClausesContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#caseClause.
func (v *Visitor) VisitCaseClause(ctx *parser.CaseClauseContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#defaultClause.
func (v *Visitor) VisitDefaultClause(ctx *parser.DefaultClauseContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#labelledStatement.
func (v *Visitor) VisitLabelledStatement(ctx *parser.LabelledStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#throwStatement.
func (v *Visitor) VisitThrowStatement(ctx *parser.ThrowStatementContext) parser.VisitorResult {
	value := v.AcceptString(ctx.ExpressionSequence())
	return parser.StringVisitorResult(fmt.Sprintf("panic(%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#tryStatement.
func (v *Visitor) VisitTryStatement(ctx *parser.TryStatementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#catchProduction.
func (v *Visitor) VisitCatchProduction(ctx *parser.CatchProductionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#finallyProduction.
func (v *Visitor) VisitFinallyProduction(ctx *parser.FinallyProductionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#debuggerStatement.
func (v *Visitor) VisitDebuggerStatement(ctx *parser.DebuggerStatementContext) parser.VisitorResult {
	return parser.StringVisitorResult("")
}

// Visit a parse tree produced by JavaScriptParser#functionDeclaration.
func (v *Visitor) VisitFunctionDeclaration(ctx *parser.FunctionDeclarationContext) parser.VisitorResult {
	if ctx.Async() != nil || ctx.Multiply() != nil {
		// TODO
		panic("not supported")
	}

	// Prepare for seeing a return statement in the current function.
	v.seenReturn = append(v.seenReturn, false)

	name := v.AcceptString(ctx.Identifier())
	parameters := v.AcceptString(ctx.FormalParameterList())
	body := v.AcceptString(ctx.FunctionBody())

	// Have we seen a return statement in the current function?
	ret := ""
	if !v.seenReturn[len(v.seenReturn)-1] {
		// It seems not, we have to inject a return statement.
		ret = "return __js.UNDEFINED\n"
	}
	v.seenReturn = v.seenReturn[:len(v.seenReturn)-1]

	return parser.StringVisitorResult(fmt.Sprintf("func %s(%s) __js.Value {\n%s\n%s}", name, parameters, body, ret))
}

// Visit a parse tree produced by JavaScriptParser#classDeclaration.
func (v *Visitor) VisitClassDeclaration(ctx *parser.ClassDeclarationContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#classTail.
func (v *Visitor) VisitClassTail(ctx *parser.ClassTailContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#classElement.
func (v *Visitor) VisitClassElement(ctx *parser.ClassElementContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#methodDefinition.
func (v *Visitor) VisitMethodDefinition(ctx *parser.MethodDefinitionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#formalParameterList.
func (v *Visitor) VisitFormalParameterList(ctx *parser.FormalParameterListContext) parser.VisitorResult {
	parameters := ""
	for _, parameter := range ctx.AllFormalParameterArg() {
		parameters = appendWithComma(parameters, v.AcceptString(parameter))
	}
	lastFormalParameterArg := ctx.LastFormalParameterArg()
	if lastFormalParameterArg != nil {
		parameters = appendWithComma(parameters, v.AcceptString(lastFormalParameterArg))
	}
	return parser.StringVisitorResult(parameters)
}

// Visit a parse tree produced by JavaScriptParser#formalParameterArg.
func (v *Visitor) VisitFormalParameterArg(ctx *parser.FormalParameterArgContext) parser.VisitorResult {
	name := v.AcceptString(ctx.Assignable())
	expression := ctx.SingleExpression()
	if expression != nil {
		// TODO: Use default value.
		_ = v.AcceptString(expression)
		panic("not supported")
	}
	return parser.StringVisitorResult(fmt.Sprintf("%s __js.Value", name))
}

// Visit a parse tree produced by JavaScriptParser#lastFormalParameterArg.
func (v *Visitor) VisitLastFormalParameterArg(ctx *parser.LastFormalParameterArgContext) parser.VisitorResult {
	// TODO: Is it correct that it is an expression here and not identifier?
	name := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("%s ...__js.Value", name))
}

// Visit a parse tree produced by JavaScriptParser#functionBody.
func (v *Visitor) VisitFunctionBody(ctx *parser.FunctionBodyContext) parser.VisitorResult {
	return v.AcceptStringVisitorResult(ctx.SourceElements())
}

// Visit a parse tree produced by JavaScriptParser#sourceElements.
func (v *Visitor) VisitSourceElements(ctx *parser.SourceElementsContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#arrayLiteral.
func (v *Visitor) VisitArrayLiteral(ctx *parser.ArrayLiteralContext) parser.VisitorResult {
	elements := v.AcceptString(ctx.ElementList())
	return parser.StringVisitorResult(fmt.Sprintf("__js.NewArray(%s)", elements))
}

// Visit a parse tree produced by JavaScriptParser#elementList.
func (v *Visitor) VisitElementList(ctx *parser.ElementListContext) parser.VisitorResult {
	elements := ""
	for _, element := range ctx.AllArrayElement() {
		elements = appendWithComma(elements, v.AcceptString(element))
	}
	return parser.StringVisitorResult(elements)
}

// Visit a parse tree produced by JavaScriptParser#arrayElement.
func (v *Visitor) VisitArrayElement(ctx *parser.ArrayElementContext) parser.VisitorResult {
	if ctx.Ellipsis() != nil {
		// TODO
		panic("not supported")
	}
	return v.AcceptStringVisitorResult(ctx.SingleExpression())
}

// Visit a parse tree produced by JavaScriptParser#PropertyExpressionAssignment.
func (v *Visitor) VisitPropertyExpressionAssignment(ctx *parser.PropertyExpressionAssignmentContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ComputedPropertyExpressionAssignment.
func (v *Visitor) VisitComputedPropertyExpressionAssignment(ctx *parser.ComputedPropertyExpressionAssignmentContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#FunctionProperty.
func (v *Visitor) VisitFunctionProperty(ctx *parser.FunctionPropertyContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#PropertyGetter.
func (v *Visitor) VisitPropertyGetter(ctx *parser.PropertyGetterContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#PropertySetter.
func (v *Visitor) VisitPropertySetter(ctx *parser.PropertySetterContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#PropertyShorthand.
func (v *Visitor) VisitPropertyShorthand(ctx *parser.PropertyShorthandContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#propertyName.
func (v *Visitor) VisitPropertyName(ctx *parser.PropertyNameContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#arguments.
func (v *Visitor) VisitArguments(ctx *parser.ArgumentsContext) parser.VisitorResult {
	result := ""
	for _, argument := range ctx.AllArgument() {
		value := v.AcceptString(argument)
		result = appendWithComma(result, value)
	}
	return parser.StringVisitorResult(result)
}

// Visit a parse tree produced by JavaScriptParser#argument.
func (v *Visitor) VisitArgument(ctx *parser.ArgumentContext) parser.VisitorResult {
	if ctx.Ellipsis() != nil {
		// TODO
		panic("not supported")
	}
	return v.AcceptStringVisitorResult(ctx.SingleExpression())
}

// Visit a parse tree produced by JavaScriptParser#expressionSequence.
func (v *Visitor) VisitExpressionSequence(ctx *parser.ExpressionSequenceContext) parser.VisitorResult {
	result := ""
	allExpressions := ctx.AllSingleExpression()
	for _, expression := range allExpressions {
		value := v.AcceptString(expression)
		result = appendWithComma(result, value)
	}
	if len(allExpressions) == 1 {
		return parser.StringVisitorResult(result)
	} else {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Sequence(%s)", result))
	}
}

// Visit a parse tree produced by JavaScriptParser#TemplateStringExpression.
func (v *Visitor) VisitTemplateStringExpression(ctx *parser.TemplateStringExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#TernaryExpression.
func (v *Visitor) VisitTernaryExpression(ctx *parser.TernaryExpressionContext) parser.VisitorResult {
	condition := v.AcceptString(ctx.SingleExpression(0))
	left := v.AcceptString(ctx.SingleExpression(1))
	right := v.AcceptString(ctx.SingleExpression(2))
	return parser.StringVisitorResult(fmt.Sprintf("func () __js.Value { if %s { return %s } else { return %s }}()", condition, left, right))
}

// Visit a parse tree produced by JavaScriptParser#LogicalAndExpression.
func (v *Visitor) VisitLogicalAndExpression(ctx *parser.LogicalAndExpressionContext) parser.VisitorResult {
	left := v.AcceptString(ctx.SingleExpression(0))
	right := v.AcceptString(ctx.SingleExpression(1))
	return parser.StringVisitorResult(fmt.Sprintf("__js.And(func () __js.Value {return %s}, func () __js.Value {return %s})", left, right))
}

// Visit a parse tree produced by JavaScriptParser#PowerExpression.
func (v *Visitor) VisitPowerExpression(ctx *parser.PowerExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#PreIncrementExpression.
func (v *Visitor) VisitPreIncrementExpression(ctx *parser.PreIncrementExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.PreIncrement(&%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#ObjectLiteralExpression.
func (v *Visitor) VisitObjectLiteralExpression(ctx *parser.ObjectLiteralExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#MetaExpression.
func (v *Visitor) VisitMetaExpression(ctx *parser.MetaExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#InExpression.
func (v *Visitor) VisitInExpression(ctx *parser.InExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#LogicalOrExpression.
func (v *Visitor) VisitLogicalOrExpression(ctx *parser.LogicalOrExpressionContext) parser.VisitorResult {
	left := v.AcceptString(ctx.SingleExpression(0))
	right := v.AcceptString(ctx.SingleExpression(1))
	return parser.StringVisitorResult(fmt.Sprintf("__js.Or(func () __js.Value {return %s}, func () __js.Value {return %s})", left, right))
}

// Visit a parse tree produced by JavaScriptParser#NotExpression.
func (v *Visitor) VisitNotExpression(ctx *parser.NotExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.Not(%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#PreDecreaseExpression.
func (v *Visitor) VisitPreDecreaseExpression(ctx *parser.PreDecreaseExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.PreDecrement(&%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#ArgumentsExpression.
func (v *Visitor) VisitArgumentsExpression(ctx *parser.ArgumentsExpressionContext) parser.VisitorResult {
	nameResult := ctx.SingleExpression().Accept(v).(parser.VisitorResult)
	arguments := v.AcceptString(ctx.Arguments())

	switch n := nameResult.(type) {
	case parser.MememberAccessVisitorResult:
		return parser.StringVisitorResult(fmt.Sprintf("__js.GoFunction(%s)(%s)", n.String(), arguments))
	default:
		return parser.StringVisitorResult(fmt.Sprintf("%s(%s)", n.String(), arguments))
	}
}

// Visit a parse tree produced by JavaScriptParser#AwaitExpression.
func (v *Visitor) VisitAwaitExpression(ctx *parser.AwaitExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ThisExpression.
func (v *Visitor) VisitThisExpression(ctx *parser.ThisExpressionContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#FunctionExpression.
func (v *Visitor) VisitFunctionExpression(ctx *parser.FunctionExpressionContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#UnaryMinusExpression.
func (v *Visitor) VisitUnaryMinusExpression(ctx *parser.UnaryMinusExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.UnaryMinus(%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#AssignmentExpression.
func (v *Visitor) VisitAssignmentExpression(ctx *parser.AssignmentExpressionContext) parser.VisitorResult {
	leftResult := ctx.SingleExpression(0).Accept(v).(parser.VisitorResult)
	right := v.AcceptString(ctx.SingleExpression(1))

	switch l := leftResult.(type) {
	case parser.MememberAccessVisitorResult:
		return parser.StringVisitorResult(fmt.Sprintf("%s.Set(%s, %s)", l.Value, l.Name, right))
	default:
		return parser.StringVisitorResult(fmt.Sprintf("__js.Assign(&%s, %s)", l.String(), right))
	}
}

// Visit a parse tree produced by JavaScriptParser#PostDecreaseExpression.
func (v *Visitor) VisitPostDecreaseExpression(ctx *parser.PostDecreaseExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.PostDecrement(&%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#TypeofExpression.
func (v *Visitor) VisitTypeofExpression(ctx *parser.TypeofExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.TypeOf(%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#InstanceofExpression.
func (v *Visitor) VisitInstanceofExpression(ctx *parser.InstanceofExpressionContext) parser.VisitorResult {
	left := v.AcceptString(ctx.SingleExpression(0))
	right := v.AcceptString(ctx.SingleExpression(1))
	return parser.StringVisitorResult(fmt.Sprintf("__js.InstanceOf(%s, %s)", left, right))
}

// Visit a parse tree produced by JavaScriptParser#UnaryPlusExpression.
func (v *Visitor) VisitUnaryPlusExpression(ctx *parser.UnaryPlusExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.UnaryPlus(%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#DeleteExpression.
func (v *Visitor) VisitDeleteExpression(ctx *parser.DeleteExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ImportExpression.
func (v *Visitor) VisitImportExpression(ctx *parser.ImportExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#EqualityExpression.
func (v *Visitor) VisitEqualityExpression(ctx *parser.EqualityExpressionContext) parser.VisitorResult {
	left := v.AcceptString(ctx.SingleExpression(0))
	right := v.AcceptString(ctx.SingleExpression(1))
	if ctx.Equals_() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.AbstractEqual(%s, %s)", left, right))
	} else if ctx.NotEquals() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.AbstractNotEqual(%s, %s)", left, right))
	} else if ctx.IdentityEquals() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.StrictEqual(%s, %s)", left, right))
	} else if ctx.IdentityNotEquals() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.StrictNotEqual(%s, %s)", left, right))
	} else {
		panic("unknown equality expression")
	}
}

// Visit a parse tree produced by JavaScriptParser#BitXOrExpression.
func (v *Visitor) VisitBitXOrExpression(ctx *parser.BitXOrExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#SuperExpression.
func (v *Visitor) VisitSuperExpression(ctx *parser.SuperExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#MultiplicativeExpression.
func (v *Visitor) VisitMultiplicativeExpression(ctx *parser.MultiplicativeExpressionContext) parser.VisitorResult {
	left := v.AcceptString(ctx.SingleExpression(0))
	right := v.AcceptString(ctx.SingleExpression(1))
	if ctx.Multiply() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Multiply(%s, %s)", left, right))
	} else if ctx.Divide() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Divide(%s, %s)", left, right))
	} else if ctx.Modulus() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Modulus(%s, %s)", left, right))
	} else {
		panic("unknown multiplicative expression")
	}
}

// Visit a parse tree produced by JavaScriptParser#BitShiftExpression.
func (v *Visitor) VisitBitShiftExpression(ctx *parser.BitShiftExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#ParenthesizedExpression.
func (v *Visitor) VisitParenthesizedExpression(ctx *parser.ParenthesizedExpressionContext) parser.VisitorResult {
	return parser.StringVisitorResult(fmt.Sprintf(`(%s)`, v.AcceptString(ctx.ExpressionSequence())))
}

// Visit a parse tree produced by JavaScriptParser#AdditiveExpression.
func (v *Visitor) VisitAdditiveExpression(ctx *parser.AdditiveExpressionContext) parser.VisitorResult {
	left := v.AcceptString(ctx.SingleExpression(0))
	right := v.AcceptString(ctx.SingleExpression(1))
	if ctx.Plus() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Plus(%s, %s)", left, right))
	} else if ctx.Minus() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Minus(%s, %s)", left, right))
	} else {
		panic("unknown multiplicative expression")
	}
}

// Visit a parse tree produced by JavaScriptParser#RelationalExpression.
func (v *Visitor) VisitRelationalExpression(ctx *parser.RelationalExpressionContext) parser.VisitorResult {
	left := v.AcceptString(ctx.SingleExpression(0))
	right := v.AcceptString(ctx.SingleExpression(1))
	if ctx.LessThan() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Less(%s, %s)", left, right))
	} else if ctx.MoreThan() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.Greater(%s, %s)", left, right))
	} else if ctx.LessThanEquals() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.LessOrEqual(%s, %s)", left, right))
	} else if ctx.GreaterThanEquals() != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.GreaterOrEqual(%s, %s)", left, right))
	} else {
		panic("unknown relational expression")
	}
}

// Visit a parse tree produced by JavaScriptParser#PostIncrementExpression.
func (v *Visitor) VisitPostIncrementExpression(ctx *parser.PostIncrementExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	return parser.StringVisitorResult(fmt.Sprintf("__js.PostIncrement(&%s)", value))
}

// Visit a parse tree produced by JavaScriptParser#YieldExpression.
func (v *Visitor) VisitYieldExpression(ctx *parser.YieldExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#BitNotExpression.
func (v *Visitor) VisitBitNotExpression(ctx *parser.BitNotExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#NewExpression.
func (v *Visitor) VisitNewExpression(ctx *parser.NewExpressionContext) parser.VisitorResult {
	name := v.AcceptString(ctx.SingleExpression())
	arguments := ctx.Arguments()
	if arguments != nil {
		return parser.StringVisitorResult(fmt.Sprintf("__js.New(%s, %s)", name, v.AcceptString(arguments)))
	} else {
		return parser.StringVisitorResult(fmt.Sprintf("__js.New(%s)", name))
	}
}

// Visit a parse tree produced by JavaScriptParser#LiteralExpression.
func (v *Visitor) VisitLiteralExpression(ctx *parser.LiteralExpressionContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#ArrayLiteralExpression.
func (v *Visitor) VisitArrayLiteralExpression(ctx *parser.ArrayLiteralExpressionContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#MemberDotExpression.
func (v *Visitor) VisitMemberDotExpression(ctx *parser.MemberDotExpressionContext) parser.VisitorResult {
	if ctx.QuestionMark() != nil || ctx.Hashtag() != nil {
		// TODO
		panic("not supported")
	}
	value := v.AcceptString(ctx.SingleExpression())
	name := v.AcceptString(ctx.IdentifierName())
	return parser.MememberAccessVisitorResult{
		Value: value,
		Name:  fmt.Sprintf(`__js.String("%s")`, name),
	}
}

// Visit a parse tree produced by JavaScriptParser#ClassExpression.
func (v *Visitor) VisitClassExpression(ctx *parser.ClassExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#MemberIndexExpression.
func (v *Visitor) VisitMemberIndexExpression(ctx *parser.MemberIndexExpressionContext) parser.VisitorResult {
	value := v.AcceptString(ctx.SingleExpression())
	index := v.AcceptString(ctx.ExpressionSequence())
	return parser.MememberAccessVisitorResult{
		Value: value,
		Name:  index,
	}
}

// Visit a parse tree produced by JavaScriptParser#IdentifierExpression.
func (v *Visitor) VisitIdentifierExpression(ctx *parser.IdentifierExpressionContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#BitAndExpression.
func (v *Visitor) VisitBitAndExpression(ctx *parser.BitAndExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#BitOrExpression.
func (v *Visitor) VisitBitOrExpression(ctx *parser.BitOrExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#AssignmentOperatorExpression.
func (v *Visitor) VisitAssignmentOperatorExpression(ctx *parser.AssignmentOperatorExpressionContext) parser.VisitorResult {
	leftResult := ctx.SingleExpression(0).Accept(v).(parser.VisitorResult)
	right := v.AcceptString(ctx.SingleExpression(1))
	operator := v.AcceptString(ctx.AssignmentOperator())

	switch l := leftResult.(type) {
	case parser.MememberAccessVisitorResult:
		return parser.StringVisitorResult(fmt.Sprintf("%[1]s.Set(%[2]s, %[3]s(%[1]s.Get(%[2]s), %[4]s))", l.Value, l.Name, operator, right))
	default:
		return parser.StringVisitorResult(fmt.Sprintf("__js.AssignWithOperator(&%s, %s, %s)", l.String(), operator, right))
	}
}

// Visit a parse tree produced by JavaScriptParser#VoidExpression.
func (v *Visitor) VisitVoidExpression(ctx *parser.VoidExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#CoalesceExpression.
func (v *Visitor) VisitCoalesceExpression(ctx *parser.CoalesceExpressionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#assignable.
func (v *Visitor) VisitAssignable(ctx *parser.AssignableContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#objectLiteral.
func (v *Visitor) VisitObjectLiteral(ctx *parser.ObjectLiteralContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#FunctionDecl.
func (v *Visitor) VisitFunctionDecl(ctx *parser.FunctionDeclContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#AnonymousFunctionDecl.
func (v *Visitor) VisitAnonymousFunctionDecl(ctx *parser.AnonymousFunctionDeclContext) parser.VisitorResult {
	if ctx.Async() != nil || ctx.Multiply() != nil {
		// TODO
		panic("not supported")
	}

	// Prepare for seeing a return statement in the current function.
	v.seenReturn = append(v.seenReturn, false)

	parameters := v.AcceptString(ctx.FormalParameterList())
	body := v.AcceptString(ctx.FunctionBody())

	// Have we seen a return statement in the current function?
	ret := ""
	if !v.seenReturn[len(v.seenReturn)-1] {
		// It seems not, we have to inject a return statement.
		ret = "return __js.UNDEFINED\n"
	}
	v.seenReturn = v.seenReturn[:len(v.seenReturn)-1]

	return parser.StringVisitorResult(fmt.Sprintf("func (%s) __js.Value {\n%s\n%s}", parameters, body, ret))
}

// Visit a parse tree produced by JavaScriptParser#ArrowFunction.
func (v *Visitor) VisitArrowFunction(ctx *parser.ArrowFunctionContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#arrowFunctionParameters.
func (v *Visitor) VisitArrowFunctionParameters(ctx *parser.ArrowFunctionParametersContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#arrowFunctionBody.
func (v *Visitor) VisitArrowFunctionBody(ctx *parser.ArrowFunctionBodyContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#assignmentOperator.
func (v *Visitor) VisitAssignmentOperator(ctx *parser.AssignmentOperatorContext) parser.VisitorResult {
	if ctx.MultiplyAssign() != nil {
		return parser.StringVisitorResult("__js.Multiply")
	} else if ctx.DivideAssign() != nil {
		return parser.StringVisitorResult("__js.Divide")
	} else if ctx.ModulusAssign() != nil {
		return parser.StringVisitorResult("__js.Modulus")
	} else if ctx.PlusAssign() != nil {
		return parser.StringVisitorResult("__js.Plus")
	} else if ctx.MinusAssign() != nil {
		return parser.StringVisitorResult("__js.Minus")
	} else if ctx.LeftShiftArithmeticAssign() != nil {
		// TODO
		panic("not supported")
	} else if ctx.RightShiftArithmeticAssign() != nil {
		// TODO
		panic("not supported")
	} else if ctx.RightShiftLogicalAssign() != nil {
		// TODO
		panic("not supported")
	} else if ctx.BitAndAssign() != nil {
		// TODO
		panic("not supported")
	} else if ctx.BitXorAssign() != nil {
		// TODO
		panic("not supported")
	} else if ctx.BitOrAssign() != nil {
		// TODO
		panic("not supported")
	} else if ctx.PowerAssign() != nil {
		// TODO
		panic("not supported")
	} else {
		// This should never happen.
		panic("unknown assignment operator")
	}
}

func (v *Visitor) convertStringLiteral(str string) string {
	// TODO: Remove quotes, unescape.
	return fmt.Sprintf("`%s`", str)
}

func (v *Visitor) convertTemplateStringLiteral(str string) string {
	// TODO: Remove quotes, unescape.
	return str
}

// Visit a parse tree produced by JavaScriptParser#literal.
func (v *Visitor) VisitLiteral(ctx *parser.LiteralContext) parser.VisitorResult {
	res := v.VisitChildren(ctx).(parser.VisitorResult).String()
	if ctx.NullLiteral() != nil {
		return parser.StringVisitorResult("__js.NULL")
	} else if ctx.BooleanLiteral() != nil {
		return parser.StringVisitorResult(fmt.Sprintf(`__js.Boolean(%s)`, res))
	} else if ctx.StringLiteral() != nil {
		return parser.StringVisitorResult(fmt.Sprintf(`__js.String(%s)`, v.convertStringLiteral(res)))
	} else if ctx.TemplateStringLiteral() != nil {
		return parser.StringVisitorResult(fmt.Sprintf(`__js.String(%s)`, v.convertTemplateStringLiteral(res)))
	} else {
		// NumericLiteral or BigintLiteral.
		return parser.StringVisitorResult(res)
	}
}

// Visit a parse tree produced by JavaScriptParser#numericLiteral.
func (v *Visitor) VisitNumericLiteral(ctx *parser.NumericLiteralContext) parser.VisitorResult {
	return parser.StringVisitorResult(fmt.Sprintf(`__js.Number(%s)`, v.VisitChildren(ctx)))
}

// Visit a parse tree produced by JavaScriptParser#bigintLiteral.
func (v *Visitor) VisitBigintLiteral(ctx *parser.BigintLiteralContext) parser.VisitorResult {
	return parser.StringVisitorResult(fmt.Sprintf(`__js.NewBigInt("%s")`, v.VisitChildren(ctx)))
}

// Visit a parse tree produced by JavaScriptParser#getter.
func (v *Visitor) VisitGetter(ctx *parser.GetterContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#setter.
func (v *Visitor) VisitSetter(ctx *parser.SetterContext) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#identifierName.
func (v *Visitor) VisitIdentifierName(ctx *parser.IdentifierNameContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#identifier.
func (v *Visitor) VisitIdentifier(ctx *parser.IdentifierContext) parser.VisitorResult {
	// TODO: Make sure identifiers do not clash.
	name := v.VisitChildren(ctx).(parser.VisitorResult).String()
	if name == "func" {
		return parser.StringVisitorResult("func_")
	}
	return parser.StringVisitorResult(name)
}

// Visit a parse tree produced by JavaScriptParser#reservedWord.
func (v *Visitor) VisitReservedWord(ctx *parser.ReservedWordContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#keyword.
func (v *Visitor) VisitKeyword(ctx *parser.KeywordContext) parser.VisitorResult {
	return v.VisitChildren(ctx).(parser.VisitorResult)
}

// Visit a parse tree produced by JavaScriptParser#let_.
func (v *Visitor) VisitLet_(ctx *parser.Let_Context) parser.VisitorResult {
	// TODO
	panic("not supported")
}

// Visit a parse tree produced by JavaScriptParser#eos.
func (v *Visitor) VisitEos(ctx *parser.EosContext) parser.VisitorResult {
	return parser.StringVisitorResult("")
}

type SyntaxError struct {
	OffendingSymbol interface{}
	Line            int
	Column          int
	Msg             string
	Exception       antlr.RecognitionException
}

type ErrorListener struct {
	SyntaxErrors []SyntaxError
}

func (l *ErrorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{}, line, column int, msg string, e antlr.RecognitionException) {
	l.SyntaxErrors = append(l.SyntaxErrors, SyntaxError{
		OffendingSymbol: offendingSymbol,
		Line:            line,
		Column:          column,
		Msg:             msg,
		Exception:       e,
	})
}

func (l *ErrorListener) ReportAmbiguity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, exact bool, ambigAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	// Ignoring.
}

func (l *ErrorListener) ReportAttemptingFullContext(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, conflictingAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	// Ignoring.
}

func (l *ErrorListener) ReportContextSensitivity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex, prediction int, configs antlr.ATNConfigSet) {
	// Ignoring.
}

func Preamble() string {
	imports := map[string]string{
		"__js":      "gitlab.com/js2go/js2go/js",
		"__console": "gitlab.com/js2go/js2go/js/console",
		"__math":    "gitlab.com/js2go/js2go/js/math",
		"__process": "gitlab.com/js2go/js2go/js/process",
	}
	preambleString := ""
	for name, path := range imports {
		preambleString += fmt.Sprintf("import %s \"%s\"\n", name, path)
	}
	preambleString += "var console __js.Value = __console.Console\n"
	preambleString += "var Math __js.Value = __math.Math\n"
	preambleString += "var process __js.Value = __process.Process\n"
	preambleString += "var undefined __js.Value = __js.UNDEFINED\n"
	return preambleString
}

func Compile(javaScriptCode string, packageName string) (string, error) {
	inputStream := antlr.NewInputStream(javaScriptCode)
	lexer := parser.NewJavaScriptLexer(inputStream)
	tokenStream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	p := parser.NewJavaScriptParser(tokenStream)
	errorListener := &ErrorListener{}
	p.RemoveErrorListeners()
	p.AddErrorListener(errorListener)
	visitor := &Visitor{}
	switch interface{}(visitor).(type) {
	case parser.JavaScriptParserVisitor:
	default:
		panic("visitor is not JavaScriptParserVisitor")
	}
	goCode := p.Program().Accept(visitor).(parser.StringVisitorResult).String()
	if len(errorListener.SyntaxErrors) > 0 || len(visitor.Errors) > 0 {
		return "", &CompileError{
			Message:       "error compiling the code",
			SyntaxErrors:  errorListener.SyntaxErrors,
			VisitorErrors: visitor.Errors,
		}
	}
	goCode = fmt.Sprintf(
		"package %s\n%s%s\nfunc init() {\n%s\n}",
		packageName,
		Preamble(),
		goCode,
		visitor.TopLevelStatements,
	)
	formattedCode, err := format.Source([]byte(goCode))
	if err != nil {
		return "", err
	}
	return string(formattedCode), nil
}
