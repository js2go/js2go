package js

import (
	"math"
	"reflect"
)

func And(leftFunc func() Value, rightFunc func() Value) Value {
	left := leftFunc()
	if !GoBool(left) {
		return left
	} else {
		return rightFunc()
	}
}

func Or(leftFunc func() Value, rightFunc func() Value) Value {
	left := leftFunc()
	if GoBool(left) {
		return left
	} else {
		return rightFunc()
	}
}

func Not(value Value) Boolean {
	return Boolean(!GoBool(value))
}

func UnaryMinus(value Value) Number {
	return Number(-GoFloat64(value))
}

func UnaryPlus(value Value) Number {
	return Number(GoFloat64(value))
}

func Multiply(left Value, right Value) Number {
	return Number(GoFloat64(left) * GoFloat64(right))
}

func Divide(left Value, right Value) Number {
	return Number(GoFloat64(left) / GoFloat64(right))
}

func Modulus(left Value, right Value) Number {
	return Number(math.Mod(GoFloat64(left), GoFloat64(right)))
}

func Plus(left Value, right Value) Value {
	left = ToPrimitiveValue(left)
	right = ToPrimitiveValue(right)

	_, leftIsString := left.(String)
	_, rightIsString := left.(String)

	if leftIsString || rightIsString {
		return String(left.String() + right.String())
	}

	return Number(GoFloat64(left) + GoFloat64(right))
}

func Minus(left Value, right Value) Number {
	return Number(GoFloat64(left) - GoFloat64(right))
}

func PreIncrement(value *Value) Number {
	result := Number(GoFloat64(*value) + 1)
	*value = result
	return result
}

func PreDecrement(value *Value) Number {
	result := Number(GoFloat64(*value) - 1)
	*value = result
	return result
}

func PostIncrement(value *Value) Number {
	temp := Number(GoFloat64(*value))
	*value = temp + 1
	return temp
}

func PostDecrement(value *Value) Number {
	temp := Number(GoFloat64(*value))
	*value = temp - 1
	return temp
}

func TypeOf(value Value) String {
	return String(value.TypeOf())
}

func InstanceOf(left Value, right Value) Boolean {
	// TODO
	panic("not supported")
}

func AbstractEqual(left Value, right Value) Boolean {
	if reflect.TypeOf(left) == reflect.TypeOf(right) {
		return StrictEqual(left, right)
	}

	if (left == NULL || left == UNDEFINED) && (right == NULL || right == UNDEFINED) {
		return true
	}

	_, leftIsNumber := left.(Number)
	_, rightIsNumber := right.(Number)
	_, leftIsString := left.(String)
	_, rightIsString := right.(String)

	if leftIsNumber && rightIsString {
		return AbstractEqual(left, Number(GoFloat64(right)))
	}
	if leftIsString && rightIsNumber {
		return AbstractEqual(Number(GoFloat64(left)), right)
	}

	_, leftIsBoolean := left.(Boolean)
	_, rightIsBoolean := right.(Boolean)

	if leftIsBoolean {
		return AbstractEqual(Number(GoFloat64(left)), right)
	}
	if rightIsBoolean {
		return AbstractEqual(left, Number(GoFloat64(right)))
	}

	// If right is an object.
	if (leftIsString || leftIsNumber) && !(rightIsString || rightIsNumber || right == NULL || right == UNDEFINED) {
		return AbstractEqual(left, ToPrimitiveValue(right))
	}

	// If left is an object.
	if !(leftIsString || leftIsNumber || left == NULL || left == UNDEFINED) && (rightIsString || rightIsNumber) {
		return AbstractEqual(ToPrimitiveValue(left), right)
	}

	return false
}

func AbstractNotEqual(left Value, right Value) Boolean {
	return !AbstractEqual(left, right)
}

func StrictEqual(left Value, right Value) Boolean {
	// For primitive values, this compares values themselves.
	// Object values are always passed as pointers, and for them it
	// compares pointers, as is should.
	// TODO: This does not work correctly on BigInt values, where "BigInt(1) === BigInt(1)" is true.
	return left == right
}

func StrictNotEqual(left Value, right Value) Boolean {
	return !StrictEqual(left, right)
}

func Less(left Value, right Value) Boolean {
	leftPrimitive := ToPrimitiveValue(left)
	rightPrimitive := ToPrimitiveValue(right)

	leftString, leftIsString := leftPrimitive.(String)
	rightString, rightIsString := rightPrimitive.(String)

	if leftIsString && rightIsString {
		return leftString < rightString
	}

	// TODO: Compare BigInts, too.
	return GoFloat64(left) < GoFloat64(right)
}

func Greater(left Value, right Value) Boolean {
	leftPrimitive := ToPrimitiveValue(left)
	rightPrimitive := ToPrimitiveValue(right)

	leftString, leftIsString := leftPrimitive.(String)
	rightString, rightIsString := rightPrimitive.(String)

	if leftIsString && rightIsString {
		return leftString > rightString
	}

	// TODO: Compare BigInts, too.
	return GoFloat64(left) > GoFloat64(right)
}

func LessOrEqual(left Value, right Value) Boolean {
	leftPrimitive := ToPrimitiveValue(left)
	rightPrimitive := ToPrimitiveValue(right)

	leftString, leftIsString := leftPrimitive.(String)
	rightString, rightIsString := rightPrimitive.(String)

	if leftIsString && rightIsString {
		return leftString <= rightString
	}

	// TODO: Compare BigInts, too.
	return GoFloat64(left) <= GoFloat64(right)
}

func GreaterOrEqual(left Value, right Value) Boolean {
	leftPrimitive := ToPrimitiveValue(left)
	rightPrimitive := ToPrimitiveValue(right)

	leftString, leftIsString := leftPrimitive.(String)
	rightString, rightIsString := rightPrimitive.(String)

	if leftIsString && rightIsString {
		return leftString >= rightString
	}

	// TODO: Compare BigInts, too.
	return GoFloat64(left) >= GoFloat64(right)
}
