package js

// TODO: This should be a map to property descriptors.
//       Which should provide additional flags about the property (e.g., it is a getter, can it be set, etc.).
type Object map[string]Value

func (*Object) TypeOf() string {
	return "object"
}

func (*Object) String() string {
	return "[object Object]"
}

func (o *Object) ToString(args ...Value) Value {
	return String(o.String())
}

func (o *Object) Get(name Value) Value {
	value, ok := (*o)[name.String()]
	if ok {
		return value
	} else {
		return UNDEFINED
	}
}

func (o *Object) Set(name Value, value Value) Value {
	(*o)[name.String()] = value
	return value
}

func NewObject() *Object {
	var object Object = map[string]Value{}
	object.Set(String("toString"), NewFunction(object.ToString))
	return &object
}
