package js

type Value interface {
	TypeOf() string
	String() string
	Get(name Value) Value
	Set(name Value, value Value) Value
}
