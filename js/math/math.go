package console

import (
	"math"

	"gitlab.com/js2go/js2go/js"
)

type math_ struct {
	js.Object
}

var Math = &math_{
	Object: map[string]js.Value{},
}

func (math_) String() string {
	return "[object Math]"
}

func init() {
	Math.Set(js.String("sqrt"), js.NewFunction(func(args ...js.Value) js.Value {
		if len(args) == 0 {
			args = append(args, js.UNDEFINED)
		}

		number := js.GoFloat64(args[0])
		return js.Number(math.Sqrt(number))
	}))
}
