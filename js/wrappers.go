package js

type BooleanWrapper struct {
	Boolean Boolean
	Object
}

func NewBooleanWrapper(boolean Boolean) *BooleanWrapper {
	return &BooleanWrapper{
		Boolean: boolean,
		Object:  map[string]Value{},
	}
}

type NumberWrapper struct {
	Number Number
	Object
}

func NewNumberWrapper(number Number) *NumberWrapper {
	wrapper := NumberWrapper{
		Number: number,
		Object: map[string]Value{
			// TODO: Implement prototypes.
			"toFixed": NewFunction(number.ToFixed),
		},
	}
	return &wrapper
}

type StringWrapper struct {
	String String
	Object
}

func NewStringWrapper(str String) *StringWrapper {
	wrapper := StringWrapper{
		String: str,
		Object: map[string]Value{
			// TODO: It should not be possible to modify the length.
			"length": Number(len(str)),
		},
	}
	return &wrapper
}
