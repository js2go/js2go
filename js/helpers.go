package js

import (
	"math"
	"math/big"
	"strconv"
)

func GoFloat64(value Value) float64 {
	switch v := value.(type) {
	case Number:
		return float64(v)
	case null:
		return 0
	case undefined:
		return math.NaN()
	case String:
		f, err := strconv.ParseFloat(string(v), 64)
		if err != nil {
			return math.NaN()
		}
		return f
	case Boolean:
		if v {
			return 1
		} else {
			return 0
		}
	case *BigInt:
		panic(NewTypeError("Cannot convert a BigInt value to a number"))
	default:
		return GoFloat64(ToPrimitiveValue(value))
	}
}

func GoBool(value Value) bool {
	switch v := value.(type) {
	case Boolean:
		return bool(v)
	case null, undefined:
		return false
	case Number:
		return !(v == 0 || math.IsNaN(float64(v)))
	case String:
		return v != ""
	case *BigInt:
		return v.Value != big.NewInt(0)
	default:
		return true
	}
}

func GoFunction(value Value) func(...Value) Value {
	function, ok := value.(*Function)
	if ok {
		return function.Function
	}

	panic(NewTypeError("Value is not a function"))
}

func IsPrimitive(value Value) bool {
	switch value.(type) {
	case Boolean, null, undefined, Number, String:
		return true
	default:
		return false
	}
}

// TODO: If value is a Date object, first do toString, then valueOf.
func ToPrimitiveValue(value Value) Value {
	if IsPrimitive(value) {
		return value
	}

	valueOf := value.Get(String("valueOf"))
	valueOfFunction, ok := valueOf.(*Function)
	if ok {
		result := valueOfFunction.Function(value)
		if IsPrimitive(result) {
			return result
		}
	}

	toString := value.Get(String("toString"))
	toStringFunction, ok := toString.(*Function)
	if ok {
		result := toStringFunction.Function(value)
		if IsPrimitive(result) {
			return result
		}
	}

	panic(NewTypeError("Cannot convert object to primitive value"))
}

func Sequence(values ...Value) Value {
	return values[len(values)-1]
}

func New(value Value, args ...Value) Object {
	// TODO
	panic("not supported")
}

func ArrayIndex(index Value) (uint32, bool) {
	number, ok := index.(Number)
	// Length is uint32, so the maximum index value is one less.
	if ok && number >= 0 && number < math.MaxUint32 {
		float := float64(number)
		index := uint32(float)
		if float64(index) == float {
			return index, true
		}
	}

	return 0, false
}

// We need this helper because assignment in Go is not an expression, but
// here we pass the right value through.
func Assign(left *Value, right Value) Value {
	*left = right
	return *left
}

// We need this helper because assignment in Go is not an expression, but
// here we pass the right value through.
func AssignWithOperator(left *Value, operator func(Value, Value) Value, right Value) Value {
	*left = operator(*left, right)
	return *left
}
