package js_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/js2go/js2go/js"
)

func TestWat(t *testing.T) {
	// [] + {} => "[object Object]"
	assert.Equal(t, js.String("[object Object]"), js.Plus(js.NewArray(), js.NewObject()))
}
