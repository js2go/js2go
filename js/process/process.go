package process

import (
	"os"

	"gitlab.com/js2go/js2go/js"
)

type process struct {
	js.Object
}

var Process = &process{
	Object: map[string]js.Value{},
}

func (process) String() string {
	return "[object process]"
}

func init() {
	argv := make([]js.Value, len(os.Args), len(os.Args))
	for i, arg := range os.Args {
		argv[i] = js.String(arg)
	}
	Process.Set(js.String("argv"), js.NewArray(argv...))
}
