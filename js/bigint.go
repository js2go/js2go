package js

import (
	"fmt"
	"math/big"
)

type BigInt struct {
	Value *big.Int
	Object
}

func (*BigInt) TypeOf() string {
	return "bigint"
}

func (b *BigInt) String() string {
	return b.Value.String() + "n"
}

func (*BigInt) Get(name Value) Value {
	return UNDEFINED
}

func (*BigInt) Set(name Value, value Value) Value {
	return value
}

func NewBigInt(bigInt string) *BigInt {
	result, ok := new(big.Int).SetString(bigInt, 0)
	if ok {
		return &BigInt{
			Value:  result,
			Object: map[string]Value{},
		}
	} else {
		panic(NewSyntaxError(fmt.Sprintf("Cannot convert %s to a BigInt", bigInt)))
	}
}
