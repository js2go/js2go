package console

import (
	"fmt"

	"gitlab.com/js2go/js2go/js"
)

type console struct {
	js.Object
}

var Console = &console{
	Object: map[string]js.Value{},
}

func (console) String() string {
	return "[object console]"
}

func init() {
	// TODO: Implement by the spec: https://console.spec.whatwg.org
	Console.Set(js.String("log"), js.NewFunction(func(args ...js.Value) js.Value {
		if len(args) > 0 {
			as := make([]interface{}, 0, len(args))
			for _, a := range args {
				as = append(as, a)
			}
			fmt.Println(as...)
		}

		return js.UNDEFINED
	}))
}
