package js

import (
	"fmt"
)

type SyntaxError struct {
	Message string
	Object
}

func (e *SyntaxError) String() string {
	return fmt.Sprintf("SyntaxError: %s", e.Message)
}

func NewSyntaxError(message string) *SyntaxError {
	return &SyntaxError{
		Message: message,
		Object:  map[string]Value{},
	}
}

type TypeError struct {
	Message string
	Object
}

func (e *TypeError) String() string {
	return fmt.Sprintf("TypeError: %s", e.Message)
}

func NewTypeError(message string) *TypeError {
	return &TypeError{
		Message: message,
		Object:  map[string]Value{},
	}
}

type RangeError struct {
	Message string
	Object
}

func (e *RangeError) String() string {
	return fmt.Sprintf("RangeError: %s", e.Message)
}

func NewRangeError(message string) *RangeError {
	return &RangeError{
		Message: message,
		Object:  map[string]Value{},
	}
}
