package js

type Function struct {
	Function func(...Value) Value
	Object
}

func (*Function) TypeOf() string {
	return "function"
}

func (f *Function) String() string {
	// TODO
	panic("not supported")
}

func NewFunction(function func(...Value) Value) *Function {
	return &Function{
		Function: function,
		Object:   map[string]Value{},
	}
}
