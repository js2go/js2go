package js

import (
	"fmt"
	"math"
)

type undefined struct{}

var UNDEFINED = undefined{}

func (undefined) TypeOf() string {
	return "undefined"
}

func (undefined) String() string {
	return "undefined"
}

func (undefined) Get(name Value) Value {
	panic(NewTypeError(fmt.Sprintf("Cannot read property '%s' of undefined", name.String())))
}

func (undefined) Set(name Value, value Value) Value {
	panic(NewTypeError(fmt.Sprintf("Cannot set property '%s' of undefined", name.String())))
}

type null struct{}

var NULL = null{}

func (null) TypeOf() string {
	return "object"
}

func (null) String() string {
	return "null"
}

func (null) Get(name Value) Value {
	panic(NewTypeError(fmt.Sprintf("Cannot read property '%s' of null", name.String())))
}

func (null) Set(name Value, value Value) Value {
	panic(NewTypeError(fmt.Sprintf("Cannot set property '%s' of null", name.String())))
}

type Boolean bool

func (Boolean) TypeOf() string {
	return "boolean"
}

func (b Boolean) String() string {
	return fmt.Sprintf("%t", b)
}

func (b Boolean) Get(name Value) Value {
	return NewBooleanWrapper(b).Get(name)
}

func (b Boolean) Set(name Value, value Value) Value {
	return NewBooleanWrapper(b).Set(name, value)
}

type Number float64

func (Number) TypeOf() string {
	return "number"
}

func (n Number) String() string {
	return fmt.Sprintf("%g", n)
}

func (n Number) Get(name Value) Value {
	return NewNumberWrapper(n).Get(name)
}

func (n Number) Set(name Value, value Value) Value {
	return NewNumberWrapper(n).Set(name, value)
}

func (n Number) ToFixed(args ...Value) Value {
	if len(args) == 0 {
		args = append(args, UNDEFINED)
	}

	digitsFloat64 := GoFloat64(args[0])
	if math.IsNaN(digitsFloat64) {
		digitsFloat64 = 0
	} else if digitsFloat64 < 0 || digitsFloat64 > 100 {
		panic(NewRangeError("toFixed() digits argument must be between 0 and 100"))
	}
	digitsInt := int(digitsFloat64)
	return String(fmt.Sprintf("%.*f", digitsInt, n))
}

type String string

func (String) TypeOf() string {
	return "string"
}

func (s String) String() string {
	return string(s)
}

func (s String) Get(name Value) Value {
	return NewStringWrapper(s).Get(name)
}

func (s String) Set(name Value, value Value) Value {
	return NewStringWrapper(s).Set(name, value)
}
