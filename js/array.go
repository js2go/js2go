package js

import (
	"math"
	"strings"
)

type Array struct {
	Elements map[uint32]Value
	Object
}

func (a *Array) String() string {
	length := uint32(a.Object["length"].(Number))
	elements := make([]string, length)
	for i := uint32(0); i < length; i++ {
		v, ok := a.Elements[i]
		if !ok {
			elements[i] = ""
		} else {
			if v == NULL || v == UNDEFINED {
				elements[i] = ""
			} else {
				elements[i] = v.String()
			}
		}
	}
	return strings.Join(elements, ",")
}

func (a *Array) ToString(args ...Value) Value {
	return String(a.String())
}

func (a *Array) Get(name Value) Value {
	index, ok := ArrayIndex(name)
	if ok {
		v, ok := a.Elements[index]
		if ok {
			return v
		} else {
			return UNDEFINED
		}
	}

	return a.Object.Get(name)
}

// TODO: Setting length to a smaller value should delete elements.
func (a *Array) Set(name Value, value Value) Value {
	index, ok := ArrayIndex(name)
	if ok {
		a.Elements[index] = value
		if uint32(a.Object["length"].(Number)) < index+1 {
			a.Object["length"] = Number(index + 1)
		}
		return value
	}

	return a.Object.Set(name, value)
}

func NewArray(elements ...Value) *Array {
	if len(elements) > math.MaxUint32 {
		panic(NewRangeError("Invalid array length"))
	}
	array := Array{
		Elements: map[uint32]Value{},
		Object: map[string]Value{
			"length": Number(0),
		},
	}
	array.Set(String("toString"), NewFunction(array.ToString))
	for i, v := range elements {
		array.Elements[uint32(i)] = v
	}
	return &array
}
