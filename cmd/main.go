package main

import (
	"flag"
	"io"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/js2go/js2go"
)

func main() {
	packageName := flag.String("name", "main", "package name to use")
	_ = packageName

	flag.Parse()

	javaScriptCode, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatalf("failed to read stdin: %s", err)
		os.Exit(1)
	}

	goCode, err := js2go.Compile(string(javaScriptCode), *packageName)
	if err != nil {
		log.Fatalf("error compiling: %s", err)
		os.Exit(2)
	}

	_, err = io.WriteString(os.Stdout, goCode)
	if err != nil {
		log.Fatalf("failed to write stdout: %s", err)
		os.Exit(3)
	}
}
