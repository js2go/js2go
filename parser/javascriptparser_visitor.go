// Code generated from antlr-grammars/javascript/javascript/JavaScriptParser.g4 by ANTLR 4.9.2. DO NOT EDIT.

package parser // JavaScriptParser

import "github.com/antlr/antlr4/runtime/Go/antlr"

// A complete Visitor for a parse tree produced by JavaScriptParser.
type JavaScriptParserVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by JavaScriptParser#program.
	VisitProgram(ctx *ProgramContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#sourceElement.
	VisitSourceElement(ctx *SourceElementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#statement.
	VisitStatement(ctx *StatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#block.
	VisitBlock(ctx *BlockContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#statementList.
	VisitStatementList(ctx *StatementListContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#importStatement.
	VisitImportStatement(ctx *ImportStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#importFromBlock.
	VisitImportFromBlock(ctx *ImportFromBlockContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#moduleItems.
	VisitModuleItems(ctx *ModuleItemsContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#importDefault.
	VisitImportDefault(ctx *ImportDefaultContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#importNamespace.
	VisitImportNamespace(ctx *ImportNamespaceContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#importFrom.
	VisitImportFrom(ctx *ImportFromContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#aliasName.
	VisitAliasName(ctx *AliasNameContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ExportDeclaration.
	VisitExportDeclaration(ctx *ExportDeclarationContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ExportDefaultDeclaration.
	VisitExportDefaultDeclaration(ctx *ExportDefaultDeclarationContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#exportFromBlock.
	VisitExportFromBlock(ctx *ExportFromBlockContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#declaration.
	VisitDeclaration(ctx *DeclarationContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#variableStatement.
	VisitVariableStatement(ctx *VariableStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#variableDeclarationList.
	VisitVariableDeclarationList(ctx *VariableDeclarationListContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#variableDeclaration.
	VisitVariableDeclaration(ctx *VariableDeclarationContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#emptyStatement_.
	VisitEmptyStatement_(ctx *EmptyStatement_Context) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#expressionStatement.
	VisitExpressionStatement(ctx *ExpressionStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ifStatement.
	VisitIfStatement(ctx *IfStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#DoStatement.
	VisitDoStatement(ctx *DoStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#WhileStatement.
	VisitWhileStatement(ctx *WhileStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ForStatement.
	VisitForStatement(ctx *ForStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ForInStatement.
	VisitForInStatement(ctx *ForInStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ForOfStatement.
	VisitForOfStatement(ctx *ForOfStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#varModifier.
	VisitVarModifier(ctx *VarModifierContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#continueStatement.
	VisitContinueStatement(ctx *ContinueStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#breakStatement.
	VisitBreakStatement(ctx *BreakStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#returnStatement.
	VisitReturnStatement(ctx *ReturnStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#yieldStatement.
	VisitYieldStatement(ctx *YieldStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#withStatement.
	VisitWithStatement(ctx *WithStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#switchStatement.
	VisitSwitchStatement(ctx *SwitchStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#caseBlock.
	VisitCaseBlock(ctx *CaseBlockContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#caseClauses.
	VisitCaseClauses(ctx *CaseClausesContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#caseClause.
	VisitCaseClause(ctx *CaseClauseContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#defaultClause.
	VisitDefaultClause(ctx *DefaultClauseContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#labelledStatement.
	VisitLabelledStatement(ctx *LabelledStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#throwStatement.
	VisitThrowStatement(ctx *ThrowStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#tryStatement.
	VisitTryStatement(ctx *TryStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#catchProduction.
	VisitCatchProduction(ctx *CatchProductionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#finallyProduction.
	VisitFinallyProduction(ctx *FinallyProductionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#debuggerStatement.
	VisitDebuggerStatement(ctx *DebuggerStatementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#functionDeclaration.
	VisitFunctionDeclaration(ctx *FunctionDeclarationContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#classDeclaration.
	VisitClassDeclaration(ctx *ClassDeclarationContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#classTail.
	VisitClassTail(ctx *ClassTailContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#classElement.
	VisitClassElement(ctx *ClassElementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#methodDefinition.
	VisitMethodDefinition(ctx *MethodDefinitionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#formalParameterList.
	VisitFormalParameterList(ctx *FormalParameterListContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#formalParameterArg.
	VisitFormalParameterArg(ctx *FormalParameterArgContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#lastFormalParameterArg.
	VisitLastFormalParameterArg(ctx *LastFormalParameterArgContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#functionBody.
	VisitFunctionBody(ctx *FunctionBodyContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#sourceElements.
	VisitSourceElements(ctx *SourceElementsContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#arrayLiteral.
	VisitArrayLiteral(ctx *ArrayLiteralContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#elementList.
	VisitElementList(ctx *ElementListContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#arrayElement.
	VisitArrayElement(ctx *ArrayElementContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PropertyExpressionAssignment.
	VisitPropertyExpressionAssignment(ctx *PropertyExpressionAssignmentContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ComputedPropertyExpressionAssignment.
	VisitComputedPropertyExpressionAssignment(ctx *ComputedPropertyExpressionAssignmentContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#FunctionProperty.
	VisitFunctionProperty(ctx *FunctionPropertyContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PropertyGetter.
	VisitPropertyGetter(ctx *PropertyGetterContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PropertySetter.
	VisitPropertySetter(ctx *PropertySetterContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PropertyShorthand.
	VisitPropertyShorthand(ctx *PropertyShorthandContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#propertyName.
	VisitPropertyName(ctx *PropertyNameContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#arguments.
	VisitArguments(ctx *ArgumentsContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#argument.
	VisitArgument(ctx *ArgumentContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#expressionSequence.
	VisitExpressionSequence(ctx *ExpressionSequenceContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#TemplateStringExpression.
	VisitTemplateStringExpression(ctx *TemplateStringExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#TernaryExpression.
	VisitTernaryExpression(ctx *TernaryExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#LogicalAndExpression.
	VisitLogicalAndExpression(ctx *LogicalAndExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PowerExpression.
	VisitPowerExpression(ctx *PowerExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PreIncrementExpression.
	VisitPreIncrementExpression(ctx *PreIncrementExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ObjectLiteralExpression.
	VisitObjectLiteralExpression(ctx *ObjectLiteralExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#MetaExpression.
	VisitMetaExpression(ctx *MetaExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#InExpression.
	VisitInExpression(ctx *InExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#LogicalOrExpression.
	VisitLogicalOrExpression(ctx *LogicalOrExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#NotExpression.
	VisitNotExpression(ctx *NotExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PreDecreaseExpression.
	VisitPreDecreaseExpression(ctx *PreDecreaseExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ArgumentsExpression.
	VisitArgumentsExpression(ctx *ArgumentsExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#AwaitExpression.
	VisitAwaitExpression(ctx *AwaitExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ThisExpression.
	VisitThisExpression(ctx *ThisExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#FunctionExpression.
	VisitFunctionExpression(ctx *FunctionExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#UnaryMinusExpression.
	VisitUnaryMinusExpression(ctx *UnaryMinusExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#AssignmentExpression.
	VisitAssignmentExpression(ctx *AssignmentExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PostDecreaseExpression.
	VisitPostDecreaseExpression(ctx *PostDecreaseExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#TypeofExpression.
	VisitTypeofExpression(ctx *TypeofExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#InstanceofExpression.
	VisitInstanceofExpression(ctx *InstanceofExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#UnaryPlusExpression.
	VisitUnaryPlusExpression(ctx *UnaryPlusExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#DeleteExpression.
	VisitDeleteExpression(ctx *DeleteExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ImportExpression.
	VisitImportExpression(ctx *ImportExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#EqualityExpression.
	VisitEqualityExpression(ctx *EqualityExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#BitXOrExpression.
	VisitBitXOrExpression(ctx *BitXOrExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#SuperExpression.
	VisitSuperExpression(ctx *SuperExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#MultiplicativeExpression.
	VisitMultiplicativeExpression(ctx *MultiplicativeExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#BitShiftExpression.
	VisitBitShiftExpression(ctx *BitShiftExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ParenthesizedExpression.
	VisitParenthesizedExpression(ctx *ParenthesizedExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#AdditiveExpression.
	VisitAdditiveExpression(ctx *AdditiveExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#RelationalExpression.
	VisitRelationalExpression(ctx *RelationalExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#PostIncrementExpression.
	VisitPostIncrementExpression(ctx *PostIncrementExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#YieldExpression.
	VisitYieldExpression(ctx *YieldExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#BitNotExpression.
	VisitBitNotExpression(ctx *BitNotExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#NewExpression.
	VisitNewExpression(ctx *NewExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#LiteralExpression.
	VisitLiteralExpression(ctx *LiteralExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ArrayLiteralExpression.
	VisitArrayLiteralExpression(ctx *ArrayLiteralExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#MemberDotExpression.
	VisitMemberDotExpression(ctx *MemberDotExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ClassExpression.
	VisitClassExpression(ctx *ClassExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#MemberIndexExpression.
	VisitMemberIndexExpression(ctx *MemberIndexExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#IdentifierExpression.
	VisitIdentifierExpression(ctx *IdentifierExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#BitAndExpression.
	VisitBitAndExpression(ctx *BitAndExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#BitOrExpression.
	VisitBitOrExpression(ctx *BitOrExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#AssignmentOperatorExpression.
	VisitAssignmentOperatorExpression(ctx *AssignmentOperatorExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#VoidExpression.
	VisitVoidExpression(ctx *VoidExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#CoalesceExpression.
	VisitCoalesceExpression(ctx *CoalesceExpressionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#assignable.
	VisitAssignable(ctx *AssignableContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#objectLiteral.
	VisitObjectLiteral(ctx *ObjectLiteralContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#FunctionDecl.
	VisitFunctionDecl(ctx *FunctionDeclContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#AnonymousFunctionDecl.
	VisitAnonymousFunctionDecl(ctx *AnonymousFunctionDeclContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#ArrowFunction.
	VisitArrowFunction(ctx *ArrowFunctionContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#arrowFunctionParameters.
	VisitArrowFunctionParameters(ctx *ArrowFunctionParametersContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#arrowFunctionBody.
	VisitArrowFunctionBody(ctx *ArrowFunctionBodyContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#assignmentOperator.
	VisitAssignmentOperator(ctx *AssignmentOperatorContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#literal.
	VisitLiteral(ctx *LiteralContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#numericLiteral.
	VisitNumericLiteral(ctx *NumericLiteralContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#bigintLiteral.
	VisitBigintLiteral(ctx *BigintLiteralContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#getter.
	VisitGetter(ctx *GetterContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#setter.
	VisitSetter(ctx *SetterContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#identifierName.
	VisitIdentifierName(ctx *IdentifierNameContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#identifier.
	VisitIdentifier(ctx *IdentifierContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#reservedWord.
	VisitReservedWord(ctx *ReservedWordContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#keyword.
	VisitKeyword(ctx *KeywordContext) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#let_.
	VisitLet_(ctx *Let_Context) VisitorResult

	// Visit a parse tree produced by JavaScriptParser#eos.
	VisitEos(ctx *EosContext) VisitorResult
}
