package parser

import (
	"fmt"
)

type VisitorResult interface {
	String() string
}

type StringVisitorResult string

func (r StringVisitorResult) String() string {
	return string(r)
}

type MememberAccessVisitorResult struct {
	Value string
	Name  string
}

func (r MememberAccessVisitorResult) String() string {
	return fmt.Sprintf("%s.Get(%s)", r.Value, r.Name)
}
