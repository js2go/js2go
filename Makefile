SHELL := /bin/bash
ANTLR4 := java -Xmx500M -cp "/usr/local/lib/antlr-4.9-complete.jar:$(CLASSPATH)" org.antlr.v4.Tool

.PHONY: fmt lint build test clean

go_binaries = js2go
benchmark_binaries = spectralnorm-go spectralnorm-gccgo spectralnorm-v8go spectralnorm-js2go spectralnorm-otto spectralnorm-goja spectralnorm-node
generated_files = parser/javascript_lexer.go parser/javascript_parser.go parser/javascriptparser_visitor.go parser/javascript_lexer_base.go parser/javascript_parser_base.go

build: $(go_binaries)

generate: $(generated_files) fmt

parser/javascript_lexer_base.go: antlr-grammars/javascript/javascript/Go/javascript_lexer_base.go
	cp antlr-grammars/javascript/javascript/Go/javascript_lexer_base.go parser/javascript_lexer_base.go

parser/javascript_parser_base.go: antlr-grammars/javascript/javascript/Go/javascript_parser_base.go
	cp antlr-grammars/javascript/javascript/Go/javascript_parser_base.go parser/javascript_parser_base.go

parser/javascript%.go: antlr-grammars/javascript/javascript/*.g4
	$(ANTLR4) -Dlanguage=Go -o parser -Xexact-output-dir -visitor -no-listener antlr-grammars/javascript/javascript/JavaScriptLexer.g4
	$(ANTLR4) -Dlanguage=Go -o parser -Xexact-output-dir -visitor -no-listener antlr-grammars/javascript/javascript/JavaScriptParser.g4
	sed -i s/\\*JavaScriptLexerBase/JavaScriptLexerBase/ parser/javascript_lexer.go
	sed -i s/interface\{\}/VisitorResult/ parser/javascriptparser_visitor.go
	rm -f parser/javascriptparser_base_visitor.go

bin_gosrcs = $(wildcard *.go parser/*.go)

js2go: generate $(bin_gosrcs)
	go build -o $@ gitlab.com/js2go/js2go/cmd

benchmark: $(benchmark_binaries)

spectralnorm-go: benchmark/go/*.go benchmark/*.go benchmark/*.js
	go build -o $@ gitlab.com/js2go/js2go/benchmark/go

spectralnorm-gccgo: benchmark/go/*.go benchmark/*.go benchmark/*.js
	go build -compiler gccgo -gccgoflags="-O3" -o $@ gitlab.com/js2go/js2go/benchmark/go

spectralnorm-v8go: benchmark/v8go/*.go benchmark/*.go benchmark/*.js
	go build -o $@ gitlab.com/js2go/js2go/benchmark/v8go

spectralnorm-js2go: js2go benchmark/js2go/*.go benchmark/*.go benchmark/*.js
	./js2go < ./benchmark/spectralnorm.js > ./benchmark/js2go/spectralnorm.go
	go build -o $@ gitlab.com/js2go/js2go/benchmark/js2go

spectralnorm-otto: benchmark/otto/*.go benchmark/*.go benchmark/*.js
	go build -o $@ gitlab.com/js2go/js2go/benchmark/otto

spectralnorm-goja: benchmark/goja/*.go benchmark/*.go benchmark/*.js
	go build -o $@ gitlab.com/js2go/js2go/benchmark/goja

spectralnorm-node: benchmark/*.js
	echo "#!/usr/bin/env node" > spectralnorm-node
	cat ./benchmark/spectralnorm.js >> spectralnorm-node
	chmod +x spectralnorm-node

audit:
	go mod tidy

# We list "parser/*.go" again so that we process potentially newly generated files.
fmt:
	gofumpt -s -w $(bin_gosrcs) parser/*.go
	goimports -w -local gitlab.com/oasislabs/auth $(bin_gosrcs) parser/*.go

lint:
	golangci-lint run --timeout 4m

test: generate
	go test -v ./...

clean:
	rm -f $(go_binaries) $(benchmark_binaries) $(generated_files) parser/*.tokens parser/*.interp
