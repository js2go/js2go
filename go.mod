module gitlab.com/js2go/js2go

go 1.16

require (
	github.com/antlr/antlr4 v0.0.0-20210311221813-5e5b6d35b418 // indirect
	github.com/dop251/goja v0.0.0-20210427212725-462d53687b0d // indirect
	github.com/dop251/goja_nodejs v0.0.0-20210225215109-d91c329300e7 // indirect
	github.com/robertkrimen/otto v0.0.0-20200922221731-ef014fd054ac
	github.com/stretchr/testify v1.7.0
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
	rogchap.com/v8go v0.5.1
)
